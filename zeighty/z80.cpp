#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <memory>

#define REGISTER_PAIR_TYPED(tx, x, ty, y) \
			union {\
				struct {\
					ty y;\
					tx x;\
				};\
				uint16_t x ## y;\
			};
#define REGISTER_PAIR(x, y) REGISTER_PAIR_TYPED(uint8_t, x, uint8_t, y)

struct Z80 {
	union Flag {
		struct {
			uint8_t c : 1;
			uint8_t n : 1;
			uint8_t v : 1;
			uint8_t x : 1;
			uint8_t h : 1;
			uint8_t y : 1;
			uint8_t z : 1;
			uint8_t s : 1;
		};
		uint8_t value;
	};
	
	struct Register_file {
		REGISTER_PAIR(b, c);
		REGISTER_PAIR(d, e);
		REGISTER_PAIR(h, l);
		REGISTER_PAIR_TYPED(uint8_t, a, Flag, f);
		//REGISTER_PAIR(w, z);
	} reg[2];
	uint16_t ix;
	uint16_t iy;
	uint16_t sp;
	uint16_t pc;

	uint8_t iff1 : 1;
	uint8_t iff2 : 1;
	uint8_t control_i;
	uint8_t control_r;

	//Temporary internal registers
	uint8_t data_reg;
	//Fake
	uint16_t data_reg16;

	enum class Register {B = 0, C, D, E, H, L, HL_ref, A, F};
	
	void reset();
	int execute(int cycles);

	uint8_t& read(uint16_t);
	uint16_t& read16(uint16_t);
	void write(uint16_t, uint8_t);
	void write16(uint16_t, uint16_t);

	uint8_t in(uint8_t);
	void out(uint8_t, uint8_t);

	int run_instruction();

	static uint8_t& reg_b(Z80& z80);
	static uint8_t& reg_c(Z80& z80);
	static uint8_t& reg_d(Z80& z80);
	static uint8_t& reg_e(Z80& z80);
	static uint8_t& reg_h(Z80& z80);
	static uint8_t& reg_l(Z80& z80);
	static uint8_t& reg_a(Z80& z80);
	static uint8_t& reg_f(Z80& z80);
	static uint8_t& reg_r(Z80& z80);
	static uint8_t& reg_i(Z80& z80);

	static uint8_t& ref_hl(Z80& z80);
	static uint8_t& ref_ix_idx(Z80& z80);
	static uint8_t& ref_iy_idx(Z80& z80);

	//IX + index
	static uint16_t& reg_ix_idx(Z80& z80);
	//IY + index
	static uint8_t& reg_iy_idx(Z80& z80);
	//byte
	static uint8_t& imm(Z80& z80);
	//word
	static uint16_t& imm16(Z80& z80);

	static uint16_t& reg_bc(Z80& z80);
	static uint16_t& reg_de(Z80& z80);
	static uint16_t& reg_hl(Z80& z80);
	static uint16_t& reg_sp(Z80& z80);
	static uint16_t& reg_ix(Z80& z80);
	static uint16_t& reg_iy(Z80& z80);

	void update_alu_flags(bool carry, uint8_t result);
	void update_alu_flags(bool carry, uint16_t result);
	void update_ld_flags(uint8_t result);
	void update_ld_flags(uint16_t result);

	void update_ex_flags();

	typedef uint8_t&(*Reg)(Z80&);
	typedef uint16_t&(*Reg16)(Z80&);

	//LD X, Y
	template<Reg r1, Reg r2>
	int ld() {
		auto result = r2(*this);
		r1(*this) = result;
		update_ld_flag(result);
		return 0;
	}

	//LD (XY), Z
	template<Reg16 ref, Reg r>
	int ld() {
		auto result = r(*this);
		write(ref(*this), result);
		return 0;
	}

	//LD XY, ZW
	template<Reg16 r1, Reg16 r2>
	int ld() {
		auto result = r2(*this);
		r1(*this) = result;
		update_ld_flag(result);
		return 0;
	}
	
	//LD XY, (word)
	template<Reg16 r>
	int ld_reg_abs() {
		auto result = read16(imm16(*this));
		r(*this) = result;
		update_ld_flag(result);
		return 0;
	}

	//LD (word), XY
	template<Reg16 r1>
	int ld_abs_reg() {
		auto result = r1(*this);
		write16(imm16(*this), result);
		update_ld_flag(result);
		return 0;
	}

	int ex_dehl();

	template<Reg16 r>
	int ex_sp() {
		auto result = r(*this);
		r(*this) = read(reg_sp(*this));
		write16(reg_sp(*this), result);
		update_ex_flag();
		return 0;
	}

	int exaf();

	int exx();

	template<Reg r>
	int add() {
		bool carry;
		uint8_t result = (reg_a(*this) += r(*this));
		update_alu_flags(carry, result);
		return 0;
	}

	template<Reg r>
	int adc() {
		bool carry;
		uint8_t result = (reg_a(*this) += r(*this) + ((reg[current].f.c) ? 1 : 0));
		update_alu_flags(carry, result);
		return 0;
	}

	template<Reg r>
	int sub() {
		bool carry;
		uint8_t result = (reg_a(*this) -= r(*this));
		update_alu_flags(carry, result);
		return 0;
	}

	template<Reg r>
	int sbc() {
		bool carry;
		uint8_t result = (reg_a(*this) -= r(*this) + ((reg[current].f.c) ? 1 : 0));
		update_alu_flags(carry, result);
		return 0;
	}

	template<Reg16 r1, Reg16 r2>
	int add() {
		bool carry;
		uint16_t result = (r1(*this) += r2(*this));
		update_alu_flags(carry, result);
		return 0;
	}

	template<Reg16 r>
	int adc() {
		bool carry;
		uint8_t result = (reg_hl(*this) += r(*this) + ((reg[current].f.c) ? 1 : 0));
		update_alu_flags(carry, result);
		return 0;
	}

	template<Reg16 r>
	int sbc() {
		bool carry;
		uint8_t result = (reg_hl(*this) -= r(*this) + ((reg[current].f.c) ? 1 : 0));
		update_alu_flags(carry, result);
		return 0;
	}

	int di();
	int ei();
	int im();
	int nop();
	int hlt();

	//INC X
	template<Reg r>
	int inc() {
		uint16_t result = r(*this) + 1;
		r(*this) = result;
		return 0;
	}

	//INC (XY)
	template<Reg16 r>
	int inc_ind() {
		uint16_t result = read(r(*this)) + 1;
		write(r(*this), result);
		return 0;
	}

	//DEC X
	template<Reg r>
	int dec() {
		uint16_t result = r(*this) - 1;
		r(*this) = result;
		return 0;
	}

	//DEC (XY)
	template<Reg16 r>
	int dec_ind() {
		uint16_t result = read(r(*this)) - 1;
		write(r(*this), result);
		return 0;
	}

	//INC XY
	template<Reg16 r>
	int inc() {
		uint32_t result = r(*this) + 1;
		r(*this) = result;
		return 0;
	}

	//DEC XY
	template<Reg16 r>
	int dec() {
		uint32_t result = r(*this) - 1;
		r(*this) = result;
		return 0;
	}

	//(???)
	int daa();
	//A <- ~A
	int cpl();
	//CF <- 1
	int scf();
	//CF <- !CF
	int ccf();
	//A <- 0 - A
	int neg();

	//Rotate...

	//RLCA

	//RRCA

	//RLA

	//RRA

	//RLD

	//RRD

	//RLC

	//RL

	//RRC

	//RR

	//Logical ops
	template<Reg r>
	int and() {
		uint8_t result = (reg_a(*this) &= r(*this));
		update_alu_flags(false, result);
		return 0;
	}

	template<Reg r>
	int xor() {
		uint8_t result = (reg_a(*this) ^= r(*this));
		update_alu_flags(false, result);
		return 0;
	}

	template<Reg r>
	int or() {
		uint8_t result = (reg_a(*this) |= r(*this));
		update_alu_flags(false, result);
		return 0;
	}

	//Compare
	template<Reg r>
	int cp() {
		uint8_t result = reg_a(*this) - r(*this);
		update_alu_flags(false, result);
		return 0;
	}

	int cpi() {
		uint8_t result = reg_a(*this) - ref_hl(*this);
		update_alu_flags(false, result);
		reg_hl(*this)++;
		reg_bc(*this)--;
		return 0;
	}

	int cpd() {
		uint8_t result = reg_a(*this) - ref_hl(*this);
		update_alu_flags(false, result);
		reg_hl(*this)--;
		reg_bc(*this)--;
		return 0;
	}

	//JP PC <- address

	//JPcond

	//JR

	//JRcond

	//DJNZ B <- B - 1; while B > 0, PC <- PC + index

	//CALL (SP-1) <- PCh;(SP-2) <- PCl; SP <- SP - 2;PC <- address

	//CALLcond

	//RET

	//RETI

	//RETN

	//RST

	//PUSH XY

	//POP XY

	//IN A,(byte)

	//IN X,(C)

	//INI	(HL) <- [C];B <- B-1;HL <- HL+1

	//INIR	(HL) <- [C];B <- B-1;HL <- HL+1; Repeat while B>0

	//IND	(HL) <- [C];B <- B-1;HL <- HL-1

	//INDR	(HL) <- [C];B <- B-1;HL <- HL-1; Repeat while B>0

	//OUT (byte),A

	//OUT (C),X

	//OUTI

	//OTIR

	//OUTD

	//OTDR

	//LDI

	//LDIR

	//LDD

	//LDDR

	//BIT n,X

	//RES n,X

	//SET n,X

	//SLA X

	//SRA X

	//SRL X
};

//=============================================================================
//Z80::
//=============================================================================
void Z80::reset() {
	memset(&reg[0], 0, sizeof(reg[0]));
	memset(&reg[1], 0, sizeof(reg[1]));
	ix = 0;
	iy = 0;
	sp = 0;
	pc = 0;
}

void update_alu_flags(bool carry, uint8_t result) {

}

void update_alu_flags(bool carry, uint16_t result) {

}

void update_ld_flag(uint8_t result) {

}

void update_ld_flag(uint16_t result) {

}

void update_ex_flag() {

}

uint8_t& Z80::read(uint16_t address) {
	data_reg = 0;
	return data_reg;
}

uint16_t& Z80::read16(uint16_t address) {
	data_reg16 = 0;
	return data_reg16;
}

void Z80::write(uint16_t address, uint8_t data) {
}

void Z80::write16(uint16_t address, uint16_t data) {

}

uint8_t Z80::in(uint8_t address) {
	return 0;
}

void Z80::out(uint8_t address, uint8_t data) {
}

uint8_t& Z80::reg_b(Z80& z80) {
	return z80.reg[0].b;
}

uint8_t& Z80::reg_c(Z80& z80) {
	return z80.reg[0].c;
}

uint8_t& Z80::reg_d(Z80& z80) {
	return z80.reg[0].d;
}

uint8_t& Z80::reg_e(Z80& z80) {
	return z80.reg[0].e;
}

uint8_t& Z80::reg_h(Z80& z80) {
	return z80.reg[0].h;
}

uint8_t& Z80::reg_l(Z80& z80) {
	return z80.reg[0].l;
}

uint8_t& Z80::ref_hl(Z80& z80) {
	return z80.read(z80.reg[0].hl);
}

uint8_t& Z80::reg_a(Z80& z80) {
	return z80.reg[0].a;
}

uint8_t& Z80::reg_f(Z80& z80) {
	return z80.reg[0].f.value;
}

uint8_t& Z80::reg_r(Z80& z80) {
	return z80.control_r;
}

uint8_t& Z80::reg_i(Z80& z80) {
	return z80.control_i;
}

uint16_t& Z80::reg_bc(Z80& z80) {
	return z80.reg[0].bc;
}

uint16_t& Z80::reg_de(Z80& z80) {
	return z80.reg[0].de;
}

uint16_t& Z80::reg_hl(Z80& z80) {
	return z80.reg[0].hl;
}



uint8_t& Z80::imm(Z80& z80) {
	return z80.read(z80.pc++);
}

//Fixed opcodes
int Z80::ex_dehl() {
	std::swap(reg[0].de, reg[0].hl);
	return 0;
}

int Z80::exx() {
	std::swap(reg[0].bc, reg[1].bc);
	std::swap(reg[0].de, reg[1].de);
	std::swap(reg[0].hl, reg[1].hl);
	return 0;
}

int Z80::exaf() {
	std::swap(reg[0].af, reg[1].af);
	return 0;
}

int Z80::di(){return 0;}
int Z80::ei(){return 0;}
int Z80::im(){return 0;}
int Z80::nop(){return 0;};
int Z80::hlt(){return 0;};

int Z80::run_instruction() {
	uint8_t opcode = read(pc++);
	int cycles;
	switch (opcode) {
	case 0x00:cycles = nop();  break;
	case 0x01:cycles = ld<reg_bc, imm>();  break;
	case 0x02:;  break;
	case 0x03:;  break;
	case 0x04:;  break;
	case 0x05:;  break;
	case 0x06:;  break;
	case 0x07:;  break;
	case 0x08:;  break;
	case 0x09:;  break;
	case 0x0a:;  break;
	case 0x0b:;  break;
	case 0x0c:;  break;
	case 0x0d:;  break;
	case 0x0e:;  break;
	case 0x0f:;  break;
	case 0x10:;  break;
	case 0x11:;  break;
	case 0x12:;  break;
	case 0x13:;  break;
	case 0x14:;  break;
	case 0x15:;  break;
	case 0x16:;  break;
	case 0x17:;  break;
	case 0x18:;  break;
	case 0x19:;  break;
	case 0x1a:;  break;
	case 0x1b:;  break;
	case 0x1c:;  break;
	case 0x1d:;  break;
	case 0x1e:;  break;
	case 0x1f:;  break;
	case 0x20:;  break;
	case 0x21:;  break;
	case 0x22:;  break;
	case 0x23:;  break;
	case 0x24:;  break;
	case 0x25:;  break;
	case 0x26:;  break;
	case 0x27:;  break;
	case 0x28:;  break;
	case 0x29:;  break;
	case 0x2a:;  break;
	case 0x2b:;  break;
	case 0x2c:;  break;
	case 0x2d:;  break;
	case 0x2e:;  break;
	case 0x2f:;  break;
	case 0x30:;  break;
	case 0x31:;  break;
	case 0x32:;  break;
	case 0x33:;  break;
	case 0x34:;  break;
	case 0x35:;  break;
	case 0x36:;  break;
	case 0x37:;  break;
	case 0x38:;  break;
	case 0x39:;  break;
	case 0x3a:;  break;
	case 0x3b:;  break;
	case 0x3c:;  break;
	case 0x3d:;  break;
	case 0x3e:;  break;
	case 0x3f:;  break;
	case 0x40:;  break;
	case 0x41:;  break;
	case 0x42:;  break;
	case 0x43:;  break;
	case 0x44:;  break;
	case 0x45:;  break;
	case 0x46:;  break;
	case 0x47:;  break;
	case 0x48:;  break;
	case 0x49:;  break;
	case 0x4a:;  break;
	case 0x4b:;  break;
	case 0x4c:;  break;
	case 0x4d:;  break;
	case 0x4e:;  break;
	case 0x4f:;  break;
	case 0x50:;  break;
	case 0x51:;  break;
	case 0x52:;  break;
	case 0x53:;  break;
	case 0x54:;  break;
	case 0x55:;  break;
	case 0x56:;  break;
	case 0x57:;  break;
	case 0x58:;  break;
	case 0x59:;  break;
	case 0x5a:;  break;
	case 0x5b:;  break;
	case 0x5c:;  break;
	case 0x5d:;  break;
	case 0x5e:;  break;
	case 0x5f:;  break;
	case 0x60:;  break;
	case 0x61:;  break;
	case 0x62:;  break;
	case 0x63:;  break;
	case 0x64:;  break;
	case 0x65:;  break;
	case 0x66:;  break;
	case 0x67:;  break;
	case 0x68:;  break;
	case 0x69:;  break;
	case 0x6a:;  break;
	case 0x6b:;  break;
	case 0x6c:;  break;
	case 0x6d:;  break;
	case 0x6e:;  break;
	case 0x6f:;  break;
	case 0x70:;  break;
	case 0x71:;  break;
	case 0x72:;  break;
	case 0x73:;  break;
	case 0x74:;  break;
	case 0x75:;  break;
	case 0x76:;  break;
	case 0x77:;  break;
	case 0x78:cycles = ld<reg_a,reg_b>();  break;
	case 0x79:cycles = ld<reg_a,reg_c>();  break;
	case 0x7a:cycles = ld<reg_a,reg_d>();  break;
	case 0x7b:cycles = ld<reg_a,reg_e>();  break;
	case 0x7c:cycles = ld<reg_a,reg_h>();  break;
	case 0x7d:cycles = ld<reg_a,reg_l>();  break;
	case 0x7e:cycles = ld<reg_a,ref_hl>(); break;
	case 0x7f:cycles = ld<reg_a,reg_a>();  break;
	case 0x80:;  break;
	case 0x81:;  break;
	case 0x82:;  break;
	case 0x83:;  break;
	case 0x84:;  break;
	case 0x85:;  break;
	case 0x86:;  break;
	case 0x87:;  break;
	case 0x88:;  break;
	case 0x89:;  break;
	case 0x8a:;  break;
	case 0x8b:;  break;
	case 0x8c:;  break;
	case 0x8d:;  break;
	case 0x8e:;  break;
	case 0x8f:;  break;
	case 0x90:;  break;
	case 0x91:;  break;
	case 0x92:;  break;
	case 0x93:;  break;
	case 0x94:;  break;
	case 0x95:;  break;
	case 0x96:;  break;
	case 0x97:;  break;
	case 0x98:;  break;
	case 0x99:;  break;
	case 0x9a:;  break;
	case 0x9b:;  break;
	case 0x9c:;  break;
	case 0x9d:;  break;
	case 0x9e:;  break;
	case 0x9f:;  break;
	case 0xa0:;  break;
	case 0xa1:;  break;
	case 0xa2:;  break;
	case 0xa3:;  break;
	case 0xa4:;  break;
	case 0xa5:;  break;
	case 0xa6:;  break;
	case 0xa7:;  break;
	case 0xa8:;  break;
	case 0xa9:;  break;
	case 0xaa:;  break;
	case 0xab:;  break;
	case 0xac:;  break;
	case 0xad:;  break;
	case 0xae:;  break;
	case 0xaf:;  break;
	case 0xb0:;  break;
	case 0xb1:;  break;
	case 0xb2:;  break;
	case 0xb3:;  break;
	case 0xb4:;  break;
	case 0xb5:;  break;
	case 0xb6:;  break;
	case 0xb7:;  break;
	case 0xb8:;  break;
	case 0xb9:;  break;
	case 0xba:;  break;
	case 0xbb:;  break;
	case 0xbc:;  break;
	case 0xbd:;  break;
	case 0xbe:;  break;
	case 0xbf:;  break;
	case 0xc0:;  break;
	case 0xc1:;  break;
	case 0xc2:;  break;
	case 0xc3:;  break;
	case 0xc4:;  break;
	case 0xc5:;  break;
	case 0xc6:;  break;
	case 0xc7:;  break;
	case 0xc8:;  break;
	case 0xc9:;  break;
	case 0xca:;  break;
	case 0xcb:;  break;
	case 0xcc:;  break;
	case 0xcd:;  break;
	case 0xce:;  break;
	case 0xcf:;  break;
	case 0xd0:;  break;
	case 0xd1:;  break;
	case 0xd2:;  break;
	case 0xd3:;  break;
	case 0xd4:;  break;
	case 0xd5:;  break;
	case 0xd6:;  break;
	case 0xd7:;  break;
	case 0xd8:;  break;
	case 0xd9:;  break;
	case 0xda:;  break;
	case 0xdb:;  break;
	case 0xdc:;  break;
	case 0xdd:;  break;
	case 0xde:;  break;
	case 0xdf:;  break;
	case 0xe0:;  break;
	case 0xe1:;  break;
	case 0xe2:;  break;
	case 0xe3:;  break;
	case 0xe4:;  break;
	case 0xe5:;  break;
	case 0xe6:;  break;
	case 0xe7:;  break;
	case 0xe8:;  break;
	case 0xe9:;  break;
	case 0xea:;  break;
	case 0xeb:;  break;
	case 0xec:;  break;
	case 0xed:;  break;
	case 0xee:;  break;
	case 0xef:;  break;
	case 0xf0:;  break;
	case 0xf1:;  break;
	case 0xf2:;  break;
	case 0xf3:;  break;
	case 0xf4:;  break;
	case 0xf5:;  break;
	case 0xf6:;  break;
	case 0xf7:;  break;
	case 0xf8:;  break;
	case 0xf9:;  break;
	case 0xfa:;  break;
	case 0xfb:;  break;
	case 0xfc:;  break;
	case 0xfd:;  break;
	case 0xfe:;  break;
	case 0xff:;  break;
	}
	return cycles;
}

int main() {
	Z80 z80;
	z80.reset();
	return 0;
}